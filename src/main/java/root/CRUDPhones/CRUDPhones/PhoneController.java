/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.CRUDPhones.CRUDPhones;

import java.util.List;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.CRUDPhones.Services.TblPhone;


/**
 *
 * @author carlos
 */
@Path ("/phones")

public class PhoneController {
    EntityManagerFactory ent = Persistence.createEntityManagerFactory("root.CRUDPhones_CRUDPhones_war_1.0-SNAPSHOTPU");
    EntityManager em;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
        em = ent.createEntityManager();
        try{
        List <TblPhone> lista = em.createNamedQuery("TblPhone.findAll").getResultList();
        return Response.ok().entity(lista).build();
        }
        catch (Exception e){
           return Response.accepted().build();
        
        }
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path ("/{buscarid}")
    public Response search (@PathParam("buscarid") int id){
        em = ent.createEntityManager();
        try{
        TblPhone resultado = em.find(TblPhone.class, id);
        em.close();
        return Response.ok(500).entity(resultado).build();
        }
        catch (Exception e){
           return Response.serverError().build();
        
        }
    }
    
    
    
    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    public String newPhone (TblPhone newPhone){
        em = ent.createEntityManager();
        em.getTransaction().begin();
        em.persist(newPhone);
        em.getTransaction().commit();
        em.close();
        
        return "Registro exitoso";
    
    }
    
    @PUT
    @Consumes (MediaType.APPLICATION_JSON)
    public Response updatePhone (TblPhone phoneUpdate) {
        em = ent.createEntityManager();    
        em.getTransaction().begin();
        em.merge(phoneUpdate);
         em.getTransaction().commit();
        em.close();
  
        return Response.ok(phoneUpdate).build();
  
    }
    
    
    @DELETE
    @Path ("/{deleteID}")
    @Produces ( {MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON} )
    public Response deleteByID (@PathParam ("deleteID") int deleteID){
        em = ent.createEntityManager();    
        em.getTransaction().begin();
        TblPhone phoneToDelete = em.getReference(TblPhone.class, deleteID);
        em.remove(phoneToDelete);
        em.getTransaction().commit();   
        em.close();
        
        return Response.ok("Registro eliminado").build();
    }
        
   
}

